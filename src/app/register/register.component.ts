import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AuthService } from '../_services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  username: any;
  password: any;
  email: any;
  isSuccessful: boolean = false;
  isSignUpFailed: boolean = false;
  errorMessage: any = '';

  constructor(private authService: AuthService) { 
    
  }

  ngOnInit(): void {
  }

  onSubmit(): void {
    console.log(this.username);
    
    this.authService.register(this.username, this.email, this.password).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }
}